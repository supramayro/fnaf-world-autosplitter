state("FNaF_World") {
  bool loading : "mmfs2.dll", 0x6FBCC;
  int frame : "FNaF_World.exe", 0xAC9AC, 0x1EC;
  byte cupcakeCounter : "FNaF_World.exe", 0xAC9B0, 0x50, 0x14, 0x10, 0xB4, 0x20A;
}

startup {
  vars.isLoading = false;

  //SETTINGS
  //FNAF 57
  settings.Add("fnaf57", false, "FNAF 57: Freddy In Space");
  settings.Add("fnaf57_cupcake", false, "Split when grabbing a Cupcake", "fnaf57");
  settings.Add("fnaf57_start", false, "Start the timer when hitting Enter", "fnaf57");
  settings.Add("fnaf57_reset", false, "Reset the timer upon dying", "fnaf57");
}

start {
  vars.isLoading = false;
  return (settings["fnaf57_start"] && old.frame == 37 && current.frame == 38);
}

reset {
  return (settings["fnaf57_reset"] && old.frame == 38 && current.frame == 39);
}

split {
  if(settings["fnaf57_cupcake"] && current.frame == 38 && old.cupcakeCounter-1 == current.cupcakeCounter) {
    return true;
  }
  return false;
}

isLoading {
  //not as simple as checking the loading flag due to established community load removal standards
  //have to pause the timer when the loading screen appears (but before the game actually starts loading)
  //unpause when game has finished loading
  if(current.loading || current.frame == 9) {
    vars.isLoading = true;
  }
  else if(old.loading && !current.loading && current.frame != 9) {
    vars.isLoading = false;
  }
  return vars.isLoading;
}
